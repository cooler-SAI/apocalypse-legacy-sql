#Decay proc changes from Corp to CorpProc
#Changes Reflected in source too
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'CorporealProc' WHERE (`Entry` = '10067') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'CorporealProc' WHERE (`Entry` = '10068') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'CorporealProc' WHERE (`Entry` = '10069') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'CorporealProc' WHERE (`Entry` = '10070') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');

#Concussion Physical to PhysicalProc
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'PhysicalProc' WHERE (`Entry` = '10057') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'PhysicalProc' WHERE (`Entry` = '10055') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
UPDATE `war_world`.`ability_damage_heals` SET `DamageType` = 'PhysicalProc' WHERE (`Entry` = '10058') and (`Index` = '1') and (`ParentCommandID` = '0') and (`ParentCommandSequence` = '0');
